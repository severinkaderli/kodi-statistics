const webpack = require("webpack");
const childProcess = require("child_process");

const commitHash = childProcess
  .execSync("git rev-parse --short HEAD")
  .toString();

module.exports = {
  publicPath: "/",
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        VERSION: JSON.stringify(require("./package.json").version),
        COMMIT_HASH: JSON.stringify(commitHash)
      })
    ]
  },
  pwa: {
    workboxOptions: {
      swDest: "sw.js",
      clientsClaim: true,
      skipWaiting: true
    }
  }
};
