import Vue from "vue";
import { Table, Field, Switch, Input, Icon } from "buefy";
import App from "./App.vue";
import router from "./router";
import "./registerServiceWorker";

Vue.use(Field);
Vue.use(Icon);
Vue.use(Input);
Vue.use(Switch);
Vue.use(Table);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
