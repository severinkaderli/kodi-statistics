import { register } from "register-service-worker";
import { Snackbar } from "buefy/dist/components/snackbar";

if (process.env.NODE_ENV === "production") {
  register(`${process.env.BASE_URL}sw.js`, {
    ready() {},
    cached() {},
    updated() {
      Snackbar.open({
        message: "A new version of this app is available.",
        type: "is-primary",
        actionText: "Reload",
        indefinite: true,
        onAction: () => {
          window.location.reload();
        }
      });
    },
    offline() {
      Snackbar.open({
        message: "App is running in offline mode.",
        type: "is-primary"
      });
    },
    error() {}
  });
}
