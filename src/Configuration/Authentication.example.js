/**
 * The API key which is used to access Google Sheets.
 *
 * @type {String}
 */
const API_KEY = "YOUR_API_KEY";

export { API_KEY };
