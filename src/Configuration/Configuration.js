import { API_KEY } from "./Authentication.js";

/**
 * The key of the spreadsheet.
 *
 * @type {String}
 */
const SPREADSHEET_KEY = "1mG9C35SwEPEmURsRvcF2Brl6UrvHagvjn3MMj35SbtQ";

/**
 * The key for the cache of the movies.
 *
 * @type {String}
 */
const MOVIE_CACHE_KEY = "movies";

/**
 * The key for the cache of the shows.
 *
 * @type {String}
 */
const SHOW_CACHE_KEY = "shows";

/**
 * How many rows to fetch from the spreadsheet.
 *
 * @type {Number}
 */
const ROWS_TO_FETCH = 1000;

/**
 * The API url which is used to access the data of the spreadsheet.
 *
 * @type {String}
 */
const MOVIE_API_URL = `https://sheets.googleapis.com/v4/spreadsheets/${SPREADSHEET_KEY}/values/A2:E${ROWS_TO_FETCH}?key=${API_KEY}`;

/**
 * The API URL which is used for the tv shows.
 *
 * @type {String}
 */
const SHOW_API_URL = `https://sheets.googleapis.com/v4/spreadsheets/${SPREADSHEET_KEY}/values/'TV Shows'!A2:G${ROWS_TO_FETCH}?key=${API_KEY}`;

export default {
  API_KEY,
  SPREADSHEET_KEY,
  MOVIE_CACHE_KEY,
  SHOW_CACHE_KEY,
  ROWS_TO_FETCH,
  MOVIE_API_URL,
  SHOW_API_URL
};
