import Vue from "vue";
import Router from "vue-router";
import MovieTable from "./Components/MovieTable.vue";
import TVShowTable from "./Components/TVShowTable.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: "/",
  linkExactActiveClass: "is-active",
  routes: [
    {
      path: "/",
      redirect: "/movies"
    },
    {
      path: "/movies",
      component: MovieTable
    },
    {
      path: "/shows",
      component: TVShowTable
    }
  ]
});
