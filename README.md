# Kodi Statistics
This is an application for listing all of the movies and tv shows on my current Kodi installation on a Raspberry Pi. It reads the data from a Google Spreadsheet which gets updated every night using a Python script on the Raspberry Pi.

The Python script can be found in its own [repository](https://gitlab.com/severinkaderli/kodi-spreadsheet-sync).

## Installation
1. `npm install`
2. Copy `./src/Configuration/Authentication.example.js` to
   `./src/Configuration/Authentication.js` and enter your Google Spreadsheet API
   key.
3. Run `npm run serve` for starting the dev server or `npm run build` for building a production build. 
